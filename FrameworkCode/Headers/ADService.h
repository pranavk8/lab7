/****************************************************************************

  Header file for AD service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServAD_H
#define ServAD_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitADService(uint8_t Priority);
bool PostADService(ES_Event_t ThisEvent);
ES_Event_t RunADService(ES_Event_t ThisEvent);
float QueryDutyVal(void);

#endif /* ServAD_H */

