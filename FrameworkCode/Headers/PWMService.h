/****************************************************************************

  Header file for PWM service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServPWM_H
#define ServPWM_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitPWMService(uint8_t Priority);
bool PostPWMService(ES_Event_t ThisEvent);
ES_Event_t RunPWMService(ES_Event_t ThisEvent);
void SetDuty(float duty);

#endif /* ServPWM_H */

