/****************************************************************************
 Module
   PWMService.c

 Revision
   1.0.1

 Description
   This is a PWM file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from PWMFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "bitdefs.h"


#include "PWMService.h"
#include "ADService.h"

/*----------------------------- Module Defines ----------------------------*/
// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerMicroS 40/32
// set 200 Hz frequency so 5mS period
#define PeriodInMicroS 170
// program generator A to go to 1 at rising comare A, 0 on falling compare A
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
// program generator B to go to 1 at rising comare B, 0 on falling compare B
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
#define BitsPerNibble 4
// we will use PWM module 0 for this demo and program it for up/down counting
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void Set0DC(void);
void Set100DC(void);
void RestoreDC(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPWMService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitPWMService(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  uint32_t DutyCycle;
  
  MyPriority = Priority;
  // start by enabling the clock to the PWM Module (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0);
  // disable the PWM while initializing
  HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0;
  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  // GenA_Normal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
  HWREG( PWM0_BASE+PWM_O_0_GENA) = GenA_Normal;
  
  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG( PWM0_BASE+PWM_O_0_LOAD) = ((PeriodInMicroS * PWMTicksPerMicroS))>>1;

  // Set the initial Duty cycle on A to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  
//	DutyCycle = QueryDutyVal();

//    if (DutyCycle <2){
//      Set0DC();
//    }
//    else if (DutyCycle >98){
//      Set100DC();
//    }
//    else{
//      RestoreDC();
//    }
//  HWREG( PWM0_BASE+PWM_O_0_CMPA) = (uint32_t)HWREG( PWM0_BASE+PWM_O_0_LOAD)*(1 - DutyCycle/100.0f);
  //HWREG( PWM0_BASE+PWM_O_0_CMPA) = HWREG( PWM0_BASE+PWM_O_0_LOAD)>>1;
 
  // enable the PWM outputs
  HWREG( PWM0_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM0EN);
  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE+GPIO_O_AFSEL) |= (BIT7HI | BIT6HI);
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6 & 7
  HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) & 0x00ffffff) + (4<<(7*BitsPerNibble)) + (4<<(6*BitsPerNibble));
  // Enable pins 6 & 7 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT7HI | BIT6HI);

  // make pins 6 & 7 on Port B into outputs
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT7HI |BIT6HI);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE+ PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/***************************************************************************
Private functions
****************************************************************************/
void Set0DC(void){
  // To program 0% DC, simply set the action on Zero to set the output to zero
  HWREG( PWM0_BASE+PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
  // don't forget to restore the proper actions when the DC drops below 100%
  // or rises above 0%
}

void Set100DC(void){
  // To program 100% DC, simply set the action on Zero to set the output to one
  HWREG( PWM0_BASE+PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ONE;

  // don't forget to restore the proper actions when the DC drops below 100%
  // or rises above 0%
}

void RestoreDC(void){
// To restore the previos DC, simply set the action back to the normal actions
 HWREG( PWM0_BASE+PWM_O_0_GENA) = GenA_Normal;
}

/****************************************************************************
 Function
     PostPWMService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostPWMService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunPWMService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunPWMService(ES_Event_t ThisEvent)
{
  uint32_t DutyCycle;
  //uint32_t MaxDuty = 100;
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
 
  return ReturnEvent;
}

void SetDuty(float duty){
  
  if (duty <2){
      Set0DC();
    }
    else if (duty >98){
      Set100DC();
    }
    else{
      RestoreDC();
    }
    HWREG( PWM0_BASE+PWM_O_0_CMPA) = (uint32_t)HWREG( PWM0_BASE+PWM_O_0_LOAD)*(1 - duty/100.0f);
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

