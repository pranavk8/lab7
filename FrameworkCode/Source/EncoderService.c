/****************************************************************************
 Module
   EncoderService.c

 Revision
   1.0.1

 Description
   This is a Encoder file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from EncoderFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "bitdefs.h"


// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "EncoderService.h"
#include "PWMService.h"
#include "ADService.h"
/*----------------------------- Module Defines ----------------------------*/
// 40,000 ticks per mS assumes a 40Mhz clock
#define TicksPerMS 40000
#define PulsePerRev 3
#define GearRatio 298
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void InitLED(void);
void InitPeriodicInt( void );
void LightLED(uint32_t speed);
void ControlLaw(void);
float clamp(float val, float clampL, float clampH);
void SetDuty(float duty);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint32_t Period, SpeedRPM, MinRPM, MaxRPM, Interval;
static uint32_t LastCapture;
static float iGain, pGain, dGain, TargetRPM;
static float RequestedDuty;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitEncoderService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitEncoderService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  InitLED();
  InitPeriodicInt();
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER0_BASE+TIMER_O_TAPR) = 0;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE+TIMER_O_TAMR) = (HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<16);
  // Enable pin on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI;

  // make pin 4 on Port C into an input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 so apppears in EN2 at bit 30
  HWREG(NVIC_EN2) |= BIT30HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  
  TargetRPM = QueryDutyVal();
  
  MinRPM = 0;
  MaxRPM = 24;
  Interval = 3;
  iGain = 0.01;
  pGain = 1.2;
  dGain = 0.0;
  SetDuty(50.0);
  
	ES_Timer_InitTimer(SpeedTimer, 200);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}
/****************************************************************************
 Function
     PostEncoderService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEncoderService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}


/****************************************************************************
 Function
     EncoderInterruptService

 Parameters
     void

 Returns
     void

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
void EncoderInterruptService(void){
  uint32_t ThisCapture;
  ES_Event_t ReturnEvent;
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER0_BASE+TIMER_O_TAR);
  Period = ThisCapture - LastCapture;
  // update LastCapture to prepare for the next edge
  LastCapture = ThisCapture;
  ReturnEvent.EventType = CALC_SPEED;
  PostEncoderService(ReturnEvent);
}

/****************************************************************************
 Function
    RunEncoderService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunEncoderService(ES_Event_t ThisEvent)
{
  uint32_t PotVal;
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  if (ThisEvent.EventType == CALC_SPEED){
		
    SpeedRPM = (60000*TicksPerMS/(PulsePerRev*Period))/GearRatio;
    LightLED(SpeedRPM);
		
  }
	if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SpeedTimer){
		HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) |= BIT2HI;
    printf("Target RPM is %u\r\n", (uint32_t)TargetRPM);
		printf("Actual speed is %u\r\n", SpeedRPM);
    printf("Duty cycle is %f\r\n", RequestedDuty);
		HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= BIT2LO;
		ES_Timer_InitTimer(SpeedTimer, 100);
	}
  
  if (ThisEvent.EventType == SPEED_UPDATE){
    
    TargetRPM = QueryDutyVal();
    //printf("Target RPM is %u\r\n", TargetRPM);
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
void InitLED(void){
  //Pins PD0, PD1, PD2, PD3, PD6, PD7, PF0, PF1 selected as LED pins
	//Pin PF2 selected as output pin for oscilloscope
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5; 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3; 
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5) {
  }
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R3) != SYSCTL_PRGPIO_R3) {
  }  
  HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
  HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI);
  
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
  HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI);

//  HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO & BIT6LO & BIT7LO);
//  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  
  HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI);
	HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= BIT2LO;
}

void LightLED(uint32_t speed){
  if (speed > (7*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= BIT0HI;
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT1LO & BIT2LO & BIT3LO & BIT6LO & BIT7LO);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  } 
  else if (speed > (6*Interval + MinRPM) &&  speed <= (7*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI);
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT2LO & BIT3LO & BIT6LO & BIT7LO);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  }
  else if (speed > (5*Interval + MinRPM) &&  speed <= (6*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI);
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT3LO & BIT6LO & BIT7LO);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  }
  else if (speed > (4*Interval + MinRPM) &&  speed <= (5*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT6LO & BIT7LO);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  }
  else if (speed > (3*Interval + MinRPM) &&  speed <= (4*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI);
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT7LO);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  }
  else if (speed > (2*Interval + MinRPM) &&  speed <= (3*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT0LO & BIT1LO);
  }
  else if (speed > (1*Interval + MinRPM) &&  speed <= (2*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) &= (BIT1LO);
  }
  else if (speed > (0*Interval + MinRPM) &&  speed <= (1*Interval + MinRPM)){
    HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT6HI | BIT7HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)) |= (BIT0HI | BIT1HI);
  }
}

void InitPeriodicInt( void ){
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0; // kill a few cycles to let the clock get going
  while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0) {
  }
  // make sure that timer (Timer B) is disabled before configuring
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // set up timer B in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE+TIMER_O_TBMR) = (HWREG(WTIMER0_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)| TIMER_TBMR_TBMR_PERIOD;
  // set timeout to 100mS
  HWREG(WTIMER0_BASE+TIMER_O_TBILR) = TicksPerMS * 2;
  // enable a local timeout interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
  // enable the Timer B in Wide Timer 0 interrupt in the NVIC // it is interrupt number 95 so appears in EN2 at bit 30
  HWREG(NVIC_EN2) |= BIT31HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to // stall while stopped by the debugger 
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL); 
  
  HWREG(NVIC_PRI23) = (HWREG(NVIC_PRI23) & ~NVIC_PRI23_INTD_M) + (1 << NVIC_PRI23_INTD_S);
}

void PeriodicIntResponse( void ){
// start by clearing the source of the interrupt
 HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
// increment our counter so that we can tell this is running
 ControlLaw();
}

void ControlLaw(void) {
  static float IntegralTerm=0.0; /* integrator control effort */
  static float RPMError; /* make static for speed */
  static float LastError; /* for Derivative Control */
  //static uint32_t ThisPeriod; /* make static for speed */
  
  
  // to allow timing this routine raise a line on entry
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT2HI;
  
  //Rpm = PER2RPM/ThisPeriod;
  RPMError = TargetRPM - SpeedRPM;
  //printf("RPM error is %f\r\n", RPMError);
  IntegralTerm += iGain * RPMError;
  IntegralTerm = clamp(IntegralTerm, 0, 100/pGain); /* anti-windup */
  RequestedDuty = (pGain*((RPMError)+ IntegralTerm));
  RequestedDuty = clamp(RequestedDuty, 0, 100);
  //printf("Requested duty is %f\r\n", RequestedDuty);
  //LastError = RPMError; // update last error
  SetDuty(RequestedDuty); // output calculated control
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT2LO; /* end of execution time */
}

// constrain val to be in range clampL to clampH
float clamp(float val, float clampL, float clampH) {
  if (val > clampH){ // if too high
    return clampH;
  }
  if (val < clampL) {// if too low
    return clampL;
  }
  return val; // if OK as-is
}


/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

